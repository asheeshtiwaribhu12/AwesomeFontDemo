﻿using System.Linq;
using Xamarin.Forms;

namespace AwesomeFontas
{
    public partial class AwesomeFontasPage : ContentPage
    {
        public AwesomeFontasPage()
        {
            InitializeComponent();
            var modules = Plugin.Iconize.Iconize.Modules;
            BindingContext = new ModuleWrapper(modules.FirstOrDefault());
        }
    }
}
