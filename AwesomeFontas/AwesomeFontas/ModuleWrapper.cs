﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using FormsPlugin.Iconize;
using Plugin.Iconize;
using Xamarin.Forms;

namespace AwesomeFontas
{
    public class ModuleWrapper : INotifyPropertyChanged
    {
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Members

        private IIconModule _module;

        #endregion Members

        #region Properties

        public ICollection<IIcon> Characters => _module.Characters;

        public String FontFamily => _module.FontFamily;


        #endregion Properties

        public ModuleWrapper(IIconModule module)
        {
            _module = module;
        }


        protected virtual void NotifyPropertyChanged([CallerMemberName] String propertyName = "") => OnPropertyChanged(new PropertyChangedEventArgs(propertyName));

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e) => PropertyChanged?.Invoke(this, e);
    }
}